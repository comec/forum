import React from 'react';
import {
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Paper
} from '@material-ui/core'

const columns = [
    {
      field: 'title',
      headerName: 'Nazwa'
    }, {
      field: 'topics',
      headerName: 'Liczba tematów',
      valueGetter: row => row.topics.length
    }
];

const CategoriesTable = ({ categories }) => {
    const rows = categories;

    return (
        <TableContainer component={Paper}>
            <Table className={'classes.table'} size="small" aria-label="a dense table">
                <TableHead>
                    <TableRow>
                        <TableCell>
                            {columns[0].headerName}
                        </TableCell>
                        <TableCell align="right">
                            {columns[1].headerName}
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    { rows.map(row => (
                        <TableRow key={row.id}>
                            <TableCell>
                                <a href={`/kategorie/${row.slug}`}>
                                    {row.title}
                                </a>
                            </TableCell>
                            <TableCell align="right">
                                {row.topics.length}
                            </TableCell>
                        </TableRow>
                    )) }
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default CategoriesTable;
