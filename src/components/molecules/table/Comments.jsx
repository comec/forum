import React from 'react';
import { Divider, Avatar, Grid, Paper } from "@material-ui/core";
import {formattedDate} from 'src/framework/date'

const imgLink = "";

const Comments = ({ comments }) => {
    const rows = comments;

    console.log("Komentarze", { comments });

    return (
        <div style={{ padding: 14 }} className="App">
            <h5>Komentarze</h5>

            {comments.map(comment => (
                <Paper style={{ padding: "40px 20px", marginTop: 10 }} key={comment.id}>
                    <Grid container wrap="nowrap" spacing={2}>
                        <Grid item>
                            <Avatar alt="Remy Sharp" src={imgLink} />
                        </Grid>
                        <Grid justifyContent="left" item xs zeroMinWidth>
                            <h4 style={{ margin: 0, textAlign: "left" }}>
                                {comment.author?.email}
                            </h4>
                            <p style={{ textAlign: "left" }}>
                                {comment.content}{" "}
                            </p>
                            <p style={{ textAlign: "left", color: "gray" }}>
                                dodany {formattedDate(comment.createdAt)}
                            </p>
                        </Grid>
                    </Grid>
                </Paper>
            ))}
        </div>
    );
}

export default Comments;
