import React from 'react';
import {
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Paper
} from '@material-ui/core'
import {formattedDate} from 'src/framework/date'

const columns = [
    {
        key: 1,
        field: 'title',
        headerName: 'Tytuł wątku',
        render: row => (<a href={`/tematy/${row.id}`}>{row.title}</a>),
        align: 'left'
    }, {
        key: 2,
        field: 'topics',
        headerName: 'Liczba komentarzy',
        valueGetter: row => row.topics ? row.topics.length : 0,
        align: 'left'
    }, {
        key: 3,
        field: 'createdAt',
        headerName: 'Utworzony',
        valueGetter: row => formattedDate(row.createdAt),
        align: 'left'
    }, {
        key: 4,
        field: 'author',
        headerName: 'Autor',
        valueGetter: row => row.author?.email,
        align: 'left'
    }
];

const TopicsTable = ({ topics }) => {
    const rows = topics;

    return (
        <TableContainer component={Paper}>
            <Table className={'classes.table'} size="small" aria-label="a dense table">
                <TableHead>
                    <TableRow>
                        { columns.map(col => (
                        <TableCell key={col.key} align={col.align} >
                            {col.headerName || ''}
                        </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    { rows.map(row => (
                        <TableRow key={row.id}>
                            { columns.map(col => (
                                <TableCell key={col.key} align={col.align}>
                                    { col.render ? (
                                        col.render(row)
                                    ) : col.valueGetter ? (
                                        col.valueGetter(row)
                                    ) : col.field ? (
                                        row[col.field]
                                    ) : '' }
                                </TableCell>
                            )) }
                        </TableRow>
                    )) }
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default TopicsTable;
