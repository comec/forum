import Header from '../components/header'
import Footer from '../components/footer'
import styles from 'src/styles/app.module.scss'

export default function Layout ({children}) {
  return (
    <div className={styles.app}>
      <Header/>
      <main>
        {children}
      </main>
      <Footer/>
    </div>
  )
}
