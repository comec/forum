import React, { useCallback, useMemo, useState } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import { Button, TextField } from '@material-ui/core'
import DataService from 'src/services/data'

const CommentForm = ({ topic, comment }) => {
    const isCreateMode = useMemo(() => {
        return !(comment && typeof comment == 'object' && Object.keys(comment).length);
    }, [comment]);

    const [success, setSuccess] = useState(false)
    const [error, setError] = useState('')

    const removeHandler = useCallback(async () => {
        if(comment && comment.id){
            const res = await DataService.removeTopic(comment.id);
            if(res.success){
                setSuccess(true)
            }
        }
    }, [comment])

    return success ? (
        <div>
            <h3>Zapisano dane</h3>
        </div>
    ) : (
        <div>
            <h3>{isCreateMode ? 'Dodaj komentarz' : 'Edycja komentarza'}</h3>
            { error && (
                <p>
                    error: {error}
                </p>
            ) }
            { !isCreateMode && (
                <>
                <Button
                    onClick={removeHandler}
                    variant="contained"
                    color="secondary"
                    size="small"
                >
                    Usuń komentarz
                </Button>
                <br /><br />
                </>
            ) }
            <Formik
                initialValues={{
                    content: isCreateMode ? '' : comment.content,
                    id: isCreateMode ? '' : comment.id,
                    topicId: topic.id
                }}
                onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);
                    try {
                        const res = isCreateMode ? await DataService.createComment(values) : await DataService.updateComment(topic.id, values);
                        if(res.success){
                            setSuccess(true)
                            if(window){
                                window.location.reload();
                            }
                        }
                        if(res.error){
                            setError(res.error)
                            setSuccess(false)
                        }
                    } catch (e) {
                        setError(e.message || String(e))
                        setSuccess(false)
                    }
                    actions.setSubmitting(false);
                }}
                >
                {props => props.isSubmitting ? (
                    <div>
                        <p>
                            Przetwarzanie danych
                        </p>
                    </div>
                ) : (
                    <Form>
                        <Field type="hidden" name="topicId" />
                        <div>
                            <TextField
                                label="Treść"
                                name="content"
                                value={props.values.content}
                                onChange={e => {
                                    props.handleChange(e)
                                }}
                                variant="outlined"
                            />
                            <ErrorMessage
                                name="content"
                            />
                        </div>
                        <br />
                        { isCreateMode ? (
                            <Field
                                type="hidden"
                                name="id"
                                disabled
                            />
                        ) : (
                        <div>
                            <TextField
                                label="ID"
                                name="id"
                                value={props.values.id}
                                onChange={e => {
                                    props.handleChange(e)
                                }}
                                variant="outlined"
                                disabled
                            />
                            <ErrorMessage
                                name="id"
                            />
                        </div>
                        ) }
                        <br />
                        <Button variant="contained" color="primary" type="submit">Dodaj komentarz</Button>
                    </Form>
                )}
                </Formik>
        </div>
    )
}

export default CommentForm
