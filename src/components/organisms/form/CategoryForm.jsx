import React, { useCallback, useMemo, useState } from 'react'
import { Formik, Form, Field, connect, ErrorMessage } from 'formik'
import { Button, TextField } from '@material-ui/core'
import DataService from 'src/services/data'
import slugify from 'slugify'

const EditCategoryForm = ({ category }) => {
    const isCreateMode = useMemo(() => {
        return !(category && typeof category == 'object' && Object.keys(category).length);
    }, [category]);

    const [success, setSuccess] = useState(false)
    const [error, setError] = useState('')

    const removeHandler = useCallback(async () => {
        if(category && category.id){
            const res = await DataService.removeCategory(category.id);
            if(res.success){
                setSuccess(true)
            }
        }
    }, [category])

    return success ? (
        <div>
            <h3>Zapisano dane</h3>
        </div>
    ) : (
        <div>
            <h3>{isCreateMode ? 'Tworzenie kategori' : 'Edycja kategori'}</h3>
            { error && (
                <p>
                    error: {error}
                </p>
            ) }
            { !isCreateMode && (
                <>
                <Button
                    onClick={removeHandler}
                    variant="contained"
                    color="secondary"
                    size="small"
                >
                    Usuń kategorię
                </Button>
                <br /><br />
                </>
            ) }
            <Formik
                initialValues={{
                    title: isCreateMode ? '' : category.title,
                    slug: isCreateMode ? '' : category.slug,
                    id: isCreateMode ? '' : category.id,
                }}
                onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);
                    try {
                        const res = await DataService.createCategory(values);
                        if(res.success){
                            setSuccess(true)
                            if(window){
                                window.location.reload();
                            }
                        }
                        if(res.error){
                            setError(res.error)
                            setSuccess(false)
                        }
                    } catch (e) {
                        setError(e.message || String(e))
                        setSuccess(false)
                    }
                    actions.setSubmitting(false);
                }}
                >
                {props => props.isSubmitting ? (
                    <div>
                        <p>
                            Przetwarzanie danych
                        </p>
                    </div>
                ) : (
                    <Form>
                        <div>
                            <TextField
                                label="Tytuł"
                                name="title"
                                value={props.values.title}
                                onChange={e => {
                                    props.handleChange(e)
                                    props.setFieldValue(
                                        'slug',
                                        slugify(e.target.value).toLowerCase()
                                    )
                                }}
                                variant="outlined"
                            />
                            <ErrorMessage
                                name="title"
                            />
                        </div>
                        <br />
                        <div>
                            <TextField
                                label="Slug"
                                name="slug"
                                value={props.values.slug}
                                onChange={e => {
                                    props.handleChange(e)
                                }}
                                variant="outlined"
                            />
                            <ErrorMessage
                                name="slug"
                            />
                        </div>
                        <br />
                        { isCreateMode ? (
                            <Field
                                type="hidden"
                                name="id"
                                disabled
                            />
                        ) : (
                        <div>
                            <TextField
                                label="ID"
                                name="id"
                                value={props.values.id}
                                onChange={e => {
                                    props.handleChange(e)
                                }}
                                variant="outlined"
                                disabled
                            />
                            <ErrorMessage
                                name="id"
                            />
                        </div>
                        ) }
                        <br />
                        <Button variant="contained" color="primary" type="submit">Utwórz</Button>
                    </Form>
                )}
                </Formik>
        </div>
    )
}

export default EditCategoryForm
