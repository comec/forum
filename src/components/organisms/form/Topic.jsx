import React, { useCallback, useMemo, useState } from 'react'
import { Formik, Form, Field, connect, ErrorMessage } from 'formik'
import { Button, TextField } from '@material-ui/core'
import DataService from 'src/services/data'
import slugify from 'slugify'

const TopicForm = ({ topic, category }) => {
    const isCreateMode = useMemo(() => {
        return !(topic && typeof topic == 'object' && Object.keys(topic).length);
    }, [topic]);

    const [success, setSuccess] = useState(false)
    const [error, setError] = useState('')

    const removeHandler = useCallback(async () => {
        if(topic && topic.id){
            const res = await DataService.removeTopic(topic.id);
            if(res.success){
                setSuccess(true)
            }
        }
    }, [topic])

    return success ? (
        <div>
            <h3>Zapisano dane</h3>
        </div>
    ) : (
        <div>
            <h3>{isCreateMode ? 'Tworzenie tematu' : 'Edycja tematu'}</h3>
            { error && (
                <p>
                    error: {error}
                </p>
            ) }
            { !isCreateMode && (
                <>
                <Button
                    onClick={removeHandler}
                    variant="contained"
                    color="secondary"
                    size="small"
                >
                    Usuń temat
                </Button>
                <br /><br />
                </>
            ) }
            <Formik
                initialValues={{
                    title: isCreateMode ? '' : topic.title,
                    content: isCreateMode ? '' : topic.content,
                    id: isCreateMode ? '' : topic.id,
                    categoryId: category.id
                }}
                onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);
                    try {
                        const res = isCreateMode ? await DataService.createTopic(values) : await DataService.updateTopic(topic.id, values);
                        if(res.success){
                            setSuccess(true)
                            if(window){
                                window.location.reload();
                            }
                        }
                        if(res.error){
                            setError(res.error)
                            setSuccess(false)
                        }
                    } catch (e) {
                        setError(e.message || String(e))
                        setSuccess(false)
                    }
                    actions.setSubmitting(false);
                }}
                >
                {props => props.isSubmitting ? (
                    <div>
                        <p>
                            Przetwarzanie danych
                        </p>
                    </div>
                ) : (
                    <Form>
                        <Field type="hidden" name="categoryId" />
                        <div>
                            <TextField
                                label="Tytuł"
                                name="title"
                                value={props.values.title}
                                onChange={e => {
                                    props.handleChange(e)
                                    props.setFieldValue(
                                        'slug'
                                    )
                                }}
                                variant="outlined"
                            />
                            <ErrorMessage
                                name="title"
                            />
                        </div>
                        <br />
                        <div>
                            <TextField
                                label="Treść"
                                name="content"
                                value={props.values.content}
                                onChange={e => {
                                    props.handleChange(e)
                                }}
                                variant="outlined"
                            />
                            <ErrorMessage
                                name="content"
                            />
                        </div>
                        <br />
                        { isCreateMode ? (
                            <Field
                                type="hidden"
                                name="id"
                                disabled
                            />
                        ) : (
                        <div>
                            <TextField
                                label="ID"
                                name="id"
                                value={props.values.id}
                                onChange={e => {
                                    props.handleChange(e)
                                }}
                                variant="outlined"
                                disabled
                            />
                            <ErrorMessage
                                name="id"
                            />
                        </div>
                        ) }
                        <br />
                        <Button variant="contained" color="primary" type="submit">Utwórz</Button>
                    </Form>
                )}
                </Formik>
        </div>
    )
}

export default TopicForm
