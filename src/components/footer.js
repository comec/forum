import Link from "next/link"
import styles from "./footer.module.css"
//import { dependencies } from "../package.json"

const content = {
  copy: 'Portal kuracjusza'
}

export default function Footer() {
  return (
    <footer className={styles.footer}>
      <hr />
      <ul className={styles.navItems}>
        <li className={styles.navItem}>
          <a href="https://e-kurort.pl" target="_blank">
            {content.copy}
          </a>
        </li>
      </ul>
    </footer>
  )
}
