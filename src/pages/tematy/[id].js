import React, { useCallback, useState } from 'react'
import Layout from 'src/components/layout'
import { PrismaClient } from '@prisma/client'
import { useSession } from 'next-auth/client'
import { Container, Grid } from '@material-ui/core'
import { Formik, Form, Field } from 'formik'
import AuthService from 'src/services/auth'
import CategoryForm from 'src/components/organisms/form/CategoryForm'
import TopicsTable from 'src/components/molecules/table/Topics'
import TopicForm from 'src/components/organisms/form/Topic'
import CommentsTable from 'src/components/molecules/table/Comments'
import CommentForm from 'src/components/organisms/form/Comment';

const Page = ({ topic }) => {
  const [ session, loading ] = useSession()

  return (
    <Layout>

        <Container>
            <h5 className="page-title">
                <small>Temat</small> {topic.title}
            </h5>
            <div dangerouslySetInnerHTML={{ __html: topic.content }} />
        </Container>

        <Container>
            <CommentsTable comments={topic.comments} />
        </Container>
        <br />

        { session?.user?.email ? (
            <Container>
                <CommentForm comment={null} topic={topic} />
            </Container>
        ) : null }

        { session?.user?.email && AuthService.isAdmin(session) ? (
            <Container>
                <TopicForm topic={topic} category={{}} />
            </Container>
        ) : null }

    </Layout>
  )
};

export const getServerSideProps = async ctx => {
    const prisma = new PrismaClient()

    const topic = await prisma.topic.findFirst({
        where: {
            id: Number(ctx.query.id)
        },
        include: {
            comments: true,
            category: true
        }
    });

    const parsedComments = await Promise.all(
        topic.comments.map(async ({ createdAt, modifiedAt, ...comment }) => {
            const author = await prisma.user.findUnique({ where: { id: comment.authorId } })
            return {
                ...comment,
                createdAt: String(createdAt),
                modifiedAt: String(modifiedAt),
                author: {
                    email: author.email
                }
            };
        })
    );

    return { props: {
        topic: {
            ...topic,
            createdAt: String(topic.createdAt),
            modifiedAt: String(topic.modifiedAt),
            comments: parsedComments
        }
    } }
}

export default Page;
