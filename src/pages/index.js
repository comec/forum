import Layout from '../components/layout'
import { PrismaClient } from '@prisma/client'
import { useSession, getSession } from 'next-auth/client'
import Button from '@material-ui/core/Button';
import { Container, Grid } from '@material-ui/core'
import { DataGrid } from '@material-ui/data-grid';
import CategoriesTable from 'src/components/molecules/table/Categories';
import AuthService from 'src/services/auth'
import CategoryForm from 'src/components/organisms/form/CategoryForm'

const Page = ({ categories }) => {
  const [ session, loading ] = useSession()

  return (
    <Layout>
      <hr />
      <Container>
        <h5 className="page-title">Kategorie</h5>
        <hr />
        <CategoriesTable
          categories={categories}
        />
      </Container>
      { AuthService.isAdmin(session) ? (
        <Container>
          <CategoryForm
            category={null}
          />
        </Container>
      ) : null }
    </Layout>
  )
};

export const getServerSideProps = async ctx => {
  const prisma = new PrismaClient()
  const categories = await prisma.category.findMany({
    include: {
      topics: true
    }
  });

  return {
    props: {
      categories: categories.map(c => ({
        ...c,
        topics: c.topics.map(t => ({
          ...t,
          createdAt: String(t.createdAt),
          modifiedAt: String(t.modifiedAt),
        }))
      }))
    }
  }
}

export default Page;
