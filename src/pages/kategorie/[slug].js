import React, { useCallback, useState } from 'react'
import Layout from 'src/components/layout'
import { PrismaClient } from '@prisma/client'
import { useSession } from 'next-auth/client'
import { Container, Grid } from '@material-ui/core'
import { Formik, Form, Field } from 'formik'
import AuthService from 'src/services/auth'
import CategoryForm from 'src/components/organisms/form/CategoryForm'
import TopicsTable from 'src/components/molecules/table/Topics'
import TopicForm from 'src/components/organisms/form/Topic'

const Page = ({ category, slug }) => {
  const [ session, loading ] = useSession()

  return (
    <Layout>

      <Container>
        <h5 className="page-title">
          <small>Kategoria</small> {category.title}
        </h5>
      </Container>

      { AuthService.isAdmin(session) && (
        <Container>
          <CategoryForm category={category} />
        </Container>
      ) }
      <br />

      <Container>
        <TopicsTable topics={category.topics} />
      </Container>
      <br />

      { session?.user?.email ? (
        <Container>
          <TopicForm topic={null} category={category} />
        </Container>
      ) : null }

    </Layout>
  )
};

export const getServerSideProps = async ctx => {
  const prisma = new PrismaClient()

  const category = await prisma.category.findFirst({
    where: {
      slug: ctx.query.slug
    },
    include: {
      topics: true
    }
  });

  return { props: {
    slug: ctx.query.slug,
    category: {
      ...category,
      topics: category.topics.map(({ createdAt, modifiedAt, ...topic }) => ({
        ...topic,
        createdAt: String(createdAt),
        modifiedAt: String(modifiedAt),
      }))
    }
  } }
}

export default Page;
