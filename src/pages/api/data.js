import { PrismaClient } from '@prisma/client'
import { useSession, getSession } from 'next-auth/client'
import { session } from 'next-auth/client'

export default async (req, res) => {
    const { body } = req;
    const data = JSON.parse(body);
    const { user } = await session({ req });

    if(!user){
        res.send(null);
        return;
    }

    const prisma = new PrismaClient()
    try {
        switch (data.action) {
            case 'create-category':
                const {
                    slug,
                    title
                } = data;
                if(!title || !slug){
                    throw new Error('Wypełnij wszystkie pola');
                }
                const created = await prisma.category.create({
                    data: {
                        title,
                        slug,
                    }
                });
                res.send(created);
                break;
            default:
                throw new Error('Nieznana akcja');
                break;
        }
    } catch (e){
        res.status(404).send(null);
        return;
    }
}
