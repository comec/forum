import { PrismaClient } from '@prisma/client'
import { useSession, getSession } from 'next-auth/client'
import { session } from 'next-auth/client'

export default async (req, res) => {
    const { body } = req;
    const data = JSON.parse(body);
    const { user } = await session({ req });

    if(!user){
        res.send(null);
        return;
    }

    const prisma = new PrismaClient()

    switch (data.action) {
        case 'create-topic':
            const {
                categoryId,
                title,
                content
            } = data;
            const created = await prisma.topic.create({
                data: {
                    title,
                    content,
                    author: {
                        connect: {
                            email: user.email
                        }
                    },
                    category: {
                        connect: {
                            id: categoryId
                        }
                    }
                }
            });
            console.log("created", created);
            res.send(created);
            break;
        default:
            res.send(null);
            break;
    }
}
