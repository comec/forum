import { PrismaClient } from '@prisma/client'
import { session as getSession } from 'next-auth/client'
import AuthService from 'src/services/auth'

export default async (req, res) => {
    try {
        const session = await getSession({ req });
        const { user } = session;
        if(!user){
            throw new Error('Unauthorized user');
        }
        if(!AuthService.isAdmin(session)){
            throw new Error('No access');
        }
        const prisma = new PrismaClient()
        const id = req.query.id;

        if(req.method === 'DELETE'){
            const deletedId = await prisma.category.delete({
                where: {
                    id,
                }
            });
            if(deletedId){
                res.send(deletedId);
                return;
            }else{
                throw new Error('Failed');
            }
        }
        if(req.method === 'POST'){
            const data = JSON.parse(req.body);
            const { title, slug } = data;
            const updatedId = await prisma.category.update({
                where: {
                    id,
                },
                data: {
                    title,
                    slug
                }
            });
            if(updatedId){
                res.send(updatedId);
                return;
            }else{
                throw new Error('Failed');
            }
        }else {
            throw new Error('');
        }
    } catch (e) {
        res.status(404).send(e.message || String(e));
        return;
    }
}
