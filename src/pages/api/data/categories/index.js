import { PrismaClient } from '@prisma/client'
import { session as getSession } from 'next-auth/client'
import AuthService from 'src/services/auth'

export default async (req, res) => {
    try {
        if(req.method === 'POST'){
            const { body } = req;
            const data = JSON.parse(body);
            const { title, slug } = data;

            console.log({
                body, data, title, slug
            });

            try {
                const session = await getSession({ req });
                const { user } = session;
                console.log({
                    body, data, title, slug, session, user
                });
                res.send({
                    body, data, title, slug, session, user
                });
                return;
            } catch (e){
                console.log({
                    e,
                    m: Object.keys(e)
                });
                res.send({
                    1: Object.keys(e),
                });
                return;
            }






            if(!user){
                console.log("! user");
                throw new Error('Unauthorized user');
            }
            if(!AuthService.isAdmin(session)){
                console.log("! AuthService.isAdmin");
                throw new Error('No access');
            }
            const prisma = new PrismaClient()
            const createdId = await prisma.category.create({
                data: {
                    title,
                    slug,
                }
            });
            if(createdId){
                res.send(createdId);
                return;
            }else{
                throw new Error('Failed');
            }
        }else {
            throw new Error('');
        }
    } catch (e) {
        res.status(401).send(e.hasOwnProperty('message') ? e.message : String(e));
        return;
    }
}
