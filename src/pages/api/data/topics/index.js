import { PrismaClient } from '@prisma/client'
import { session as getSession } from 'next-auth/client'
import AuthService from 'src/services/auth'

export default async (req, res) => {
    try {
        if(req.method === 'POST'){
            const { body } = req;
            const data = JSON.parse(body);
            const { title, content, categoryId } = data;
            const session = await getSession({ req });
            const { user } = session;
            if(!user){
                throw new Error('Unauthorized user');
            }
            const prisma = new PrismaClient()
            const createdId = await prisma.topic.create({
                data: {
                    title,
                    content,
                    author: {
                        connect: {
                            email: user.email
                        }
                    },
                    category: {
                        connect: {
                            id: categoryId
                        }
                    }
                }
            });
            if(createdId){
                res.send(createdId);
                return;
            }else{
                throw new Error('Failed');
            }
        }else {
            throw new Error('');
        }
    } catch (e) {
        res.status(404).send(e.message || String(e));
        return;
    }
}
