import { PrismaClient } from '@prisma/client'
import { session as getSession } from 'next-auth/client'
import AuthService from 'src/services/auth'

export default async (req, res) => {
    try {
        if(req.method === 'POST'){
            const { body } = req;
            const data = JSON.parse(body);
            const { content, topicId } = data;
            const session = await getSession({ req });
            const { user } = session;
            if(!user){
                throw new Error('Unauthorized user');
            }
            const prisma = new PrismaClient()
            const createdId = await prisma.comment.create({
                data: {
                    content,
                    author: {
                        connect: {
                            email: user.email
                        }
                    },
                    topic: {
                        connect: {
                            id: topicId
                        }
                    }
                }
            });
            if(createdId){
                res.send(createdId);
                return;
            }else{
                throw new Error('Failed');
            }
        }else {
            throw new Error('');
        }
    } catch (e) {
        res.status(404).send(e.message || String(e));
        return;
    }
}
