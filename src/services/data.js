class Response {
    constructor(){
        this.error = '';
        this.success = false;
    }
}

const config = {
    apiUrl: `/api/data`
}

class DataService {
    constructor(){
        if(!DataService.instance){
            DataService.instance = this;
        }
        return DataService.instance;
    }

    /**
     * --------------------------------
     * PRIVATE METHODS
     * --------------------------------
     */

    /**
     * common create
     */
    _create = async (namespace, data) => {
        const response = new Response();
        try {
            const res = await fetch(`${config.apiUrl}/${namespace}`, {
                method: 'POST',
                body: JSON.stringify(data)
            }).then(r => r.json())
            if(res.id){
                response.success = true;
                response.createdId = res.id;
            }
        } catch (e) {
            response.success = false;
            response.error = e.message || String(e);
        }
        return response;
    }

    /**
     * common update
     */
    _update = async (namespace, id, data) => {
        const response = new Response();
        try {
            const res = fetch(`${config.apiUrl}/${namespace}/${id}`, {
                method: 'POST',
                body: JSON.stringify(data)
            }).then(r => r.json())
            if(res.success){
                response.success = true;
                response.updatedId = res.updatedId;
            }
        } catch (e) {
            response.success = false;
            response.error = e.message || String(e);
        }
        return response;
    }

    /**
     * common remove
     */
    _remove = async (namespace, id) => {
        const response = new Response();
        try {
            const res = fetch(`${config.apiUrl}/${namespace}/${id}`, {
                method: 'DELETE'
            }).then(r => r.json())
            if(res.success){
                response.success = true;
                response.removedId = res.removedId;
            }
        } catch (e) {
            response.success = false;
            response.error = e.message || String(e);
        }
        return response;
    }

    /**
     * --------------------------------
     * CATEGORIES
     * --------------------------------
     */

    /**
     * create category
     */
    createCategory = async (data) => {
        return this._create('categories', data);
    }

    /**
     * update category
     */
    updateCategory = async (id, data) => {
        return this._update('categories', id, data);
    }

    /**
     * Remove category
     */
    removeCategory = async id => {
        return this._remove('categories', id);
    }

    /**
     * --------------------------------
     * TOPICS
     * --------------------------------
     */

    /**
     * create topic
     */
    createTopic = async (data) => {
        return this._create('topics', data);
    }

    /**
     * update topic
     */
    updateTopic = async (data) => {
        return this._update('topics', data);
    }

    /**
     * remove topic
     */
    removeTopic = async (data) => {
        return this._remove('topics', data);
    }

    /**
     * --------------------------------
     * COMMENT
     * --------------------------------
     */

    /**
     * create comment
     */
    createComment = async (data) => {
        return this._create('comments', data);
    }

    /**
     * update comment
     */
    updateComment = async (data) => {
        return this._update('comments', data);
    }

    /**
     * remove comment
     */
    removeComment = async (data) => {
        return this._remove('comments', data);
    }
}

const instance = new DataService();
Object.freeze(instance)

export default instance
