const config = {
    adminEmails: [
        'zaczkiewicz.radoslaw@gmail.com',
        'ekurorty@gmail.com'
    ]
}

class AuthService {
    constructor(){
        if(!AuthService.instance){
            AuthService.instance = this;
        }
        return AuthService.instance;
    }

    isAdmin = session => {
        try {
            if(config.adminEmails.includes(session.user.email)){
                return true;
            }
            throw new Error ('no admin');
        } catch (e) {
            return false;
        }
    }
}

const instance = new AuthService();
Object.freeze(instance);

export default instance
