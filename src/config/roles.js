export const ROLE_ADMIN = `ADMIN`;
export const ROLE_MODERATOR = `MODERATOR`;
export const ROLE_USER = `USER`;
