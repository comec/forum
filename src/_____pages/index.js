import { useSession, getSession } from 'next-auth/client'
import { PrismaClient } from '@prisma/client'

const Page = ({ categories }) => {
  const [ session, loading ] = useSession()
  console.log("CAT", categories);

  return (
    <div>
        <h1>Sanatoria forum</h1>
    </div>
  )
};


export const getServerSideProps = async ctx => {
  const prisma = new PrismaClient()
  const categories = await prisma.category.findMany({
    include: {
      topics: true
    }
  });

  return {
    props: {
      categories: categories.map(c => ({
        ...c,
        topics: c.topics.map(t => ({
          ...t,
          createdAt: String(t.createdAt),
          modifiedAt: String(t.modifiedAt),
        }))
      }))
    }
  }
}

export default Page;
