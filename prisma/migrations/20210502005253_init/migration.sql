/*
  Warnings:

  - You are about to drop the column `parentId` on the `Post` table. All the data in the column will be lost.
  - You are about to drop the column `parentId` on the `Topic` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `Post` DROP COLUMN `parentId`;

-- AlterTable
ALTER TABLE `Topic` DROP COLUMN `parentId`;
